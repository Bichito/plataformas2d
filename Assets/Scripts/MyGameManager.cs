﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour
{
    private int lives;
    private int timecounter;


    public Text livesText;
    public Text timetext;
    public Text recordText;
    public AudioSource gameover;
    public AudioSource sound;
    public GameObject pauseMenu;
    public GameObject gameoverPanel;

    private static MyGameManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        sound.Play();
    }

    public static MyGameManager getInstance()
    {
        return instance;
    }

    // Use this for initialization
    void Start()
    {
        lives = 3;
        //highscore++;
        //highscoreText.text = highscore.ToString("D5");
        //livesText.text = "x3";
        livesText.text = "x " + lives.ToString();
        timecounter = 0;
        timetext.text = timecounter.ToString("D5");
    }
    void Update()
    {
        timecounter++;
        timetext.text = timecounter.ToString();
        if (timecounter > 999999)
        {
            sound.Stop();
            gameoverPanel.SetActive(true);
            gameover.Play();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }
    public void LoseLive()
    {
        lives--;
        livesText.text = "x " + lives.ToString();
        if (lives < 0)
        {
            sound.Stop();
            gameoverPanel.SetActive(true);
            recordText.text = "Record: " + ConvertFloatToTime(PlayerPrefs.GetFloat("Record"));
            gameover.Play();
        }
    }

    // public void AddHighscore(int value)
    // {
    // highscore += value;
    // highscoreText.text = highscore.ToString();
    //}

    public void Pause()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");
    }



    string ConvertFloatToTime(float time)
    {
        float h = time / 3600;
        time %= 3600;
        float m = time / 60;
        time %= 60;
        float s = time;

        return h.ToString("00") + ":" + m.ToString("00") + ":" + s.ToString("00");
    }

}
