﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDown : MonoBehaviour
{
    public Transform target;
    public float speed;

    private float timecounter;
    // Use this for initialization
    void Start()
    {
        if (target != null)
        {
            target.parent = null;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            timecounter++;
            if (timecounter > 240)
            {
                float fixedSpeed = speed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, target.position, fixedSpeed);
            }
        }
    }
    private void FixedUpdate()
    {

    }
}
