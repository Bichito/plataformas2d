﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed;
    public float maxSpeed;
    public float minSpeed;
    public bool grounded;
    public bool pressed;
    public float jumpPower;
    public AudioSource footstep;
    

    private float timecounter;
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool jump;

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));
        anim.SetBool("Grounded", grounded);
        

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            jump = true;
            //timecounter++;
            //if (timecounter < 100)
            //{
            //anim.SetBool("Pressed", pressed);
            //pressed = true;
            //}
            //if (timecounter > 100)
            //{
            // pressed = false;
            //timecounter = 0;
            //}
        }


    }
    private void FixedUpdate()
    {
        Vector3 fixedVelocity = rb2d.velocity;
        fixedVelocity.x *= 0.75f;

        if (grounded)
        {
            rb2d.velocity = fixedVelocity;
        }

        float h = Input.GetAxis("Horizontal");
        rb2d.AddForce(Vector2.right * speed * h);

        float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
        rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);

        if (h > 0.1f)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            if (grounded)
            {
                footstep.Stop();
            }
        }
        if (h < -0.1f)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            if (grounded)
            {
                footstep.Stop();
            }
        }
        if (jump)
        {
            footstep.Stop();
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            rb2d.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            jump = false;
        }
        Debug.Log(rb2d.velocity.x);
    }
    void OnBecameInvisible()
    {
        transform.position = new Vector3(-2, 0, 0);
        MyGameManager.getInstance().LoseLive();
    }
}

