﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	public void Exit()
    {
        Application.Quit();
    }

    public void Play()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Gameplay");
    }
    public void Options()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Options");
    }
    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
