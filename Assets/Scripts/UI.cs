﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public GameObject gameoverPanel;

    public Text timetext;
    public Text recordText;


    public void OpenGameoverPanel()
    {

        gameoverPanel.SetActive(true);
    }
    public void CloseGameoverPanel() { gameoverPanel.SetActive(false); }

    public void UpdateTimeText(float timeCounter)
    {
        Debug.Log("UpdateTimeText");
        timetext.text = ConvertFloatToTime(timeCounter);
    }

    public void UpdateGameOverPanel()
    {
        recordText.text = "Record: " + ConvertFloatToTime(PlayerPrefs.GetFloat("Record"));
    }

    string ConvertFloatToTime(float time)
    {
        float h = time / 3600;
        time %= 3600;
        float m = time / 60;
        time %= 60;
        float s = time;

        return h.ToString("00") + ":" + m.ToString("00") + ":" + s.ToString("00");
    }

}
