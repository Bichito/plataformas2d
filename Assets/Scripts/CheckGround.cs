﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    private PlayerController player;
    private Rigidbody2D rb2d;
    public GameObject winpanel;
    public AudioSource soundwin;
    // Use this for initialization
    void Start ()
    {
        player = GetComponentInParent<PlayerController>();
        rb2d = GetComponentInParent<Rigidbody2D>();
	}
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            rb2d.velocity = new Vector3 (0f, 0f, 0f);
            player.transform.parent = collision.transform;
            player.grounded = true;
        }
        if (collision.gameObject.tag == "Trampa")
        {
            Explode();
        }
        if (collision.gameObject.tag == "Win")
        {

            winpanel.SetActive(true);
            soundwin.Play();
        }
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            player.grounded = true;
        }
        if (collision.gameObject.tag == "Platform")
        {
            player.transform.parent = collision.transform;
            player.grounded = true;
        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            player.grounded = false;
        }
        if (collision.gameObject.tag == "Platform")
        {
            player.transform.parent = null;
            player.grounded = false;
        }
        //if (collision.gameObject.tag == "Ground")
        //{
        // player.pressed = false;
        //}
    }
    private void Explode()
    {
        //Perdemos 1 vida
        MyGameManager.getInstance().LoseLive();

        //A los 2 segundos Reset
        Invoke("Reset", 0.1f);
    }

    private void Reset()
    {
        player.transform.position = new Vector3(-2, 0, 0);
    }
}
